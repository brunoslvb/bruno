<?php
include_once 'Component.php';

class ModalDialog extends Component {
    

    // Variáveis privadas responsáveis pelo conteúdo do componente
    private $title = 'Modal title';
    private $content = '...';
    private $size = 'modal-md';
    private $position = 'modal-top';
    private $button_classes = '';
    private $button = '';
    private $button_texto = '';


    /** 
     * Gera o código HTML do Modal Dialog
     * @return string: código html
     */
    public function getHTML(){
        $html = $this->setHeader();
        $html .= $this->body();
        $html .= $this->footer();
        return $html;
    }


    /** 
     * Define o tamanho do modal: 
     * @param string: Tamanho do modal utilizando as classes
     * do MDBootstrap.
     * modal-md
     * modal-sm
     * modal-lg
     */ 
    public function setSize($size){
        $this->size = '';
        $this->size = $size;
        return $this;
    }


	/**
     * Define a posição do modal: 
     * @param string: Posição do modal utilizando as classes
     * do MDBootstrap.
     * modal-bottom
     * modal-top
     * modal-right
     * modal-left
     */
    public function setPosition($pos){
        $this->position = '';
        $this->position = $pos;
        return $this;
    }


    /**
     * Gera o conteúdo do Modal Dialog
     * @return string = código HTML
     */
    public function setContent($content){
        $this->content = '';
        $this->content = $content;
        return $this;
    }


    /**
     * Gera o cabeçalho do Modal Dialog
     * @return string = código HTML
     */
    private function setHeader(){
        $html = '<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">';
        $html .= '<div class="modal-dialog modal-side '.$this->size.' '.$this->position.'" role="document">';
        $html .= '<div class="modal-content">';
        $html .= '<div class="modal-header">';
        $html .= '<h5 class="modal-title" id="modal">'.$this->title.'</h5>';
        $html .= '<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span>
	        	  </button>';
        $html .= '</div>';
        return $html;
    }

    /**
     * Gera o corpo do Modal Dialog
     * @return string = código HTML
     */
    private function body(){
        $html = '<div class="modal-body">';
        $html .= $this->content;
        $html .= '</div>';
        
        return $html;
    }

    /**
     * Gera o rodapé do Modal Dialog
     * @return string = código HTML
     */
    private function footer(){
    	$html = '<div class="modal-footer">'.$this->button.'</div>';
    	$html .= '</div>';
    	$html .= '</div>';
    	$html .= '</div>';
    	return $html;
    }

    /**
     * Adiciona um botão no footer concatenando com a
     * variável privada <b>button_classes</b> que contém as classes de estilização do botão e
     * a variável <b>button_texto</b> contendo o título do botão
     */
    public function button(){
    	$this->button .= '<button type="button" class="'.$this->button_classes.'">'.$this->button_texto.'</button>';
    	return $this;
    }


    /**
     * Define o título do Modal Dialog.
     * @param string
     */
    public function setTitle($title){
        $this->title = $title;
        return $this;
    }

    /**
     * Define as classes de estilização do botão.
     * @param string
     */
    public function addButtonClass($class){
    	$this->button_classes = '';
        $this->button_classes .= "$class ";
        return $this;
    }

    
    /**
     * Define o título do botão.
     * @param string
     */
    public function addButtonText($text){
    	$this->button_texto = '';
        $this->button_texto .= $text;
        return $this;
    }

    /**
     * Retorna o tamanho do modal
     * @return string
     */
    public function getSize(){
        return $this->size;
    }

    /**
     * Retorna a posição do modal
     * @return string
     */
    public function getPosition(){
        return $this->position;
    }

    /**
     * Retorna o título do modal
     * @return string
     */
    public function getTitle(){
        return $this->title;
    }

}