<?php
include_once 'Component.php';

class ModalBox extends Component {
    
    // Variáveis privadas responsáveis pelo conteúdo do componente
    private $title = 'Modal title';
    private $content = '...';
    private $size = 'modal-md';
    private $position = 'modal-top';
    private $button_classes = '';
    private $button = '';
    private $button_texto = '';
    private $footer = '';
    private $href = '!#';
    private $background = '';



    /** 
     * Gera o código HTML do Modal Box
     * @return string: código HTML
     */ 
    public function getHTML(){
        $html = $this->css();
        $html .= $this->header();
        $html .= $this->body();
        $html .= $this->footer();
        $html .= $this->js();
        return $html;
    }


    /**
    * Método responsável por incluir o css 
    * utilizado no Modal Box na página HTML
    */
    private function css(){
        
        $html = '<style type="text/css">  

                    /* The Modal (background) */
                    .modal {
                      display: none; /* Hidden by default */
                      position: fixed; /* Stay in place */
                      z-index: 1; /* Sit on top */
                      padding-top: 100px; /* Location of the box */
                      left: 0;
                      top: 0;
                      width: 100%; /* Full width */
                      height: 100%; /* Full height */
                      overflow: auto; /* Enable scroll if needed */
                      background-color: rgb(0,0,0); /* Fallback color */
                      background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                    }

                    /* Modal Content */
                    .modal-content {
                      position: relative;
                      background-color: #fefefe;
                      margin: auto;
                      padding: 0;
                      border: 1px solid #888;
                      width: 80%;
                      box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
                      -webkit-animation-name: animatetop;
                      -webkit-animation-duration: 0.4s;
                      animation-name: animatetop;
                      animation-duration: 0.4s
                    }

                    /* Add Animation */
                    @-webkit-keyframes animatetop {
                      from {top:-300px; opacity:0} 
                      to {top:0; opacity:1}
                    }

                    @keyframes animatetop {
                      from {top:-300px; opacity:0}
                      to {top:0; opacity:1}
                    }

                    /* The Close Button */
                    .close {
                      color: white;
                      float: right;
                      font-size: 28px;
                      font-weight: bold;
                    }

                    .close:hover,
                    .close:focus {
                      color: #000;
                      text-decoration: none;
                      cursor: pointer;
                    }

                    .modal-header {
                      display: block;
                      padding: 5px 16px;
                      background-color: #'.$this->background.';
                      color: white;
                    }

                    .modal-body {
                        padding: 2px 16px;
                    }

                    .modal-footer {
                      display: block;
                      padding: 8px 8px 8px 8px;
                      background-color: #'.$this->background.';
                      color: white;
                    }

                    a{
                        text-decoration: none;
                        color: #000;
                    }

                </style>';

        return $html;
    }

    /**
    * Método responsável por incluir o javascript 
    * utilizado no Modal Box na página HTML
    */
    private function js(){
        $html = '<script type="text/javascript">  

                    var modal = document.getElementById("myModal");

                    var btn = document.getElementById("myBtn");

                    var span = document.getElementsByClassName("close")[0];

                    btn.onclick = function() {
                      modal.style.display = "block";
                    }

                    span.onclick = function() {
                      modal.style.display = "none";
                    }

                    window.onclick = function(event) {
                      if (event.target == modal) {
                        modal.style.display = "none";
                      }
                    }

                </script>';

        return $html;
    }


    /**
    * Reponsável por gerar o cabeçalho do Modal Box
    * concatenando a váriavel privada <b>title</b> 
    * contendo o título do modal
    * @return string = código HTML
    */
    private function header(){
        $html = '<div id="myModal" class="modal">';
        $html .=    '<div class="modal-content">';
        $html .=        '<div class="modal-header">';
        $html .=            '<span class="close">&times;</span>';
        $html .=            '<h2>'.$this->title.'</h2>';
        $html .=        '</div>';
        return $html;
    }

    /**
     * Reponsável por gerar o corpo do Modal Box
     * concatenando a váriavel privada <b>content</b>
     * contendo o conteúdo do modal.
     * @return string = código HTML
     */
    private function body(){
        $html =         '<div class="modal-body">';
        $html .=            $this->content;
        $html .=        '</div>';
        
        return $html;
    }

    /**
     * Reponsável por gerar o corpo do Modal Box
     * concatenando a váriavel privada <b>footer</b>
     * contendo o título do footer e a variável <b>button</b>
     * contendo os botões adicionados
     * @return string = código HTML
     */
    private function footer(){
        $html =         '<div class="modal-footer">';
        $html .=           '<h3>'.$this->footer.'</h3>';
        $html .=           $this->button;
        $html .=        '</div>';
        $html .=    '</div>';
        $html .= '</div>';
        return $html;
    }


    /**
     * Define o título do header
     * @param string
     */
    public function setTitle($title){
        $this->title = $title;
        return $this;
    }

    /**
     * Define a cor de fundo do header e footer do modal
     * @param string = Hexadecimal
     */
    public function setBackground($bg){
        $this->background = $bg;
        return $this;
    }

    /**
     * Define o conteúdo do Modal Box
     * @param string = Qualquer texto ou conteúdo HTML
     */
    public function setContent($content){
        $this->content = $content;
        return $this;
    }

    /**
     * Define o título do footer
     * @param string
     */
    public function setFooterText($footer){
        $this->footer = $footer;
        return $this;
    }

    /**
     * Adiciona um botão no footer concatenando com a
     * variável privada <b>href</b> que contém o link do botão e
     * a variável <b>button_texto</b> contendo o título do botão
     */
    public function button(){
    	$this->button .= '<button><a href="'.$this->href.'">'.$this->button_texto.'</button>';
    	return $this;
    }

    /**
     * Define o título do header
     * @param string
     */
    public function addButtonText($text){
    	$this->button_texto = $text;
        return $this;
    }

    /**
     * Define o link de redirecionamento do botão
     * @param string = url
     */
    public function setHref($href){
        $this->href = $href;
        return $this;
    }

    /**
     * Retorna a string do Href
     * @return string
     */
    public function getHref(){
        return $this->href;
    }

    /**
     * Retorna o conteúdo do background
     * @return string
     */
    public function getbackground(){
        return $this->background;
    }

    /**
     * Retorna a string do título
     * @return string
     */
    public function getTitle(){
        return $this->title;
    }

}