<?php
include_once 'Component.php';

class InputField extends Component {
    
    // Variáveis privadas responsáveis pelo conteúdo do componente
    private $button_classes = 'btn btn-primary btn-sm mt-3';
    private $button = '';
    private $button_texto = 'Ala';
    private $action = '#';
    private $form = false;
    private $label = 'Default';
    private $fields = '';
    private $lableHeader = 'Form';


    /** 
     * Gera o código HTML do formulário/campos de entrada
     * @return string: código html
     */ 
    public function getHTML(){
        $html = '';
        if ($this->form == true) {
            $html .= $this->form();
        } else {
            $html .= $this->fields;
        }
        return $html;
    }

    /** 
     * Método responsável por incluir uma caixa de 
     * formulário.
     */ 
    public function setForm(){
        $this->form = true;
        return $this;
    }

    /** 
     * Gera o formulário contendo o <b>form action</b>, 
     * <b>título do header</b>, <b>campos de entrada do formulário</b>
     * e os <b>botões de envio</b>
     * @return string = código HTML
     */ 
    private function form(){
        $html = '<form class="border border-light p-5" method="POST" action="'.$this->action.'">';
        $html .= '<p class="h4 text-center">'.$this->lableHeader.'</p>';
        $html .= $this->fields;
        $html .= $this->button;
        $html .= '</form>';

        return $html;
    }


    /** 
     * Gera do código de um campo de entrada do tipo <b>texto</b>
     * contendo o título do campo.
     * @param string = Será incluído no atributo <b>id</b> e <b>name</b>.
     */ 
    public function inputText($id){
        $this->fields .= '<label class="mt-3">'.$this->label.'</label>';
        $this->fields .= '<input type="text" id="'.$id.'" name="'.$id.'" class="form-control">';

        return $this;
    }

    /** 
     * Gera do código de um campo de entrada do tipo <b>number</b> 
     * contendo o título do campo.
     * @param string = Será incluído no atributo <b>id</b> e <b>name</b>.
     */ 
    public function inputNumber($id){
        $this->fields .= '<label>'.$this->label.'</label>';
        $this->fields .= '<input type="number" id="'.$id.'" name="'.$id.'" class="form-control">';

        return $this;
    }

    /** 
     * Gera do código de um campo de entrada do tipo <b>date</b> 
     * contendo o título do campo.
     * @param string = Será incluído no atributo <b>id</b> e <b>name</b>.
     */ 
    public function inputDate($id){
        $this->fields .= '<label>'.$this->label.'</label>';
        $this->fields .= '<input type="date" id="'.$id.'" name="'.$id.'" class="form-control">';

        return $this;
    }

    /** 
     * Gera do código de um campo de entrada do tipo <b>file</b> 
     * contendo o título do campo.
     * @param string = Será incluído no atributo <b>id</b> e <b>name</b>.
     */ 
    public function inputFile($id){
        $this->fields .= '<label>'.$this->label.'</label>';
        $this->fields .= '<input type="file" id="'.$id.'" name="'.$id.'" class="form-control">';

        return $this;
    }

    /** 
     * Define o título do campo de entrada.
     * @param string
     */ 
    public function inputLabel($label){
        $this->label = $label;
        return $this;
    }

    /** 
     * Define o link do atributo <b>action</b>.
     * @param string
     */ 
    public function formAction($action){
        $this->action = $action;
        return $this;
    }

    /** 
     * Define o título do formulário.
     * @param string
     */ 
    public function labelHeader($label){
        $this->labelHeader = $label;
        return $this;
    }

    /** 
     * Adiciona um botão de envio com classes e título.
     */ 
    public function addButton(){
        $this->button .= '<button type="submit" class="'.$this->button_classes.'">'.$this->button_texto.'</button>';
        return $this;
    }

    /**
     * Define o título do botão
     * @param string
     */
    public function addButtonText($text){
    	$this->button_texto = $text;
        return $this;
    }

    /** 
     * Define a classe que o botão herdará.
     * @param string
     */ 
    public function addButtonClass($class){
        $this->button_classes = "$class ";
        return $this;
    }


    /** 
     * Retorna o título do botão
     * @return string
     */ 
    public function getButtonText(){
        return $this->button_texto;
    }

    /** 
     * Retorna o título do cabeçalho
     * @return string
     */ 
    public function getlabelHeader(){
        return $this->labelHeader;
    }

    /** 
     * Retorna o link do form action
     * @return string
     */ 
    public function getformAction(){
        return $this->action;
    }
}