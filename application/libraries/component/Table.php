<?php
include_once 'Component.php';

class Table extends Component {
    private $data;
    private $labels;

    /** Contém as classes do cabeçalho */
    private $header_classes = '';

    /** Contém as classes da tabela */
    private $table_classes = '';
    
    public function __construct($data, $labels){
        $this->labels = $labels;
        $this->data = $data; 
    }

    /** 
     * Gera o código HTML da tabela
     * @return string: código html
     */ 
    public function getHTML(){
        $html = $this->header();
        $html .= $this->body();
        return $html. '</table>';
    }

    /**
     * Gera o cabeçalho da tabela
     * @return string
     */
    private function header(){
        $html = '<table class="table '.$this->table_classes.'">';
        $html .= '<thead class="'.$this->header_classes.'"><tr>';
        foreach ($this->labels as $rotulo) {
            $html .= "<th>$rotulo</th>";            
        }
        $html .= '</tr></thead>';
        return $html;
    }

    /**
     * Gera o corpo da tabela
     * @return string
     */
    private function body(){
        $html = '';
        foreach ($this->data as $row) {
            $html .= '<tr>';
            foreach ($row as $key => $value) {
                if($key == 'id') continue;
                $html .= "<td>$value</td>";
            }
            $html .= '</tr>';
        }
        return $html;
    }

    /**
     * Insere classes no cabeçalho da tabela
     * @param class: lista de strings separadas por espaço 
     */
    public function addHeaderClass($class){
        $this->header_classes .= "$class ";
        return $this;
    }

    /**
     * Exibe listras na tabela
     */
    public function useZebra(){
        $this->table_classes .= 'table-striped ';
        return $this;
    }

    /**
     * Exibe bordas na tabela
     */
    public function useBorder(){
        $this->table_classes .= 'table-bordered ';
        return $this;
    }

    /**
     * Inclui o efeito de mouseover nas linhas da tabela
     */
    public function useHover(){
        $this->table_classes .= 'table-hover ';
        return $this;
    }

    /**
     * Diminui a altura das linhas da tabela
     */
    public function smallRow(){
        $this->table_classes .= 'table-sm ';
        return $this;
    }

}