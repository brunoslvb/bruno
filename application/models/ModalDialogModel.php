<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/component/ModalDialog.php';

class ModalDialogModel extends CI_Model {

    public function modal(){
        $this->load->library('component/ModalDialog', null, 'modal');

        $modal = new ModalDialog();
        $modal->setContent("TESTE")->setSize('modal-sm')->setPosition('modal-right')->addButtonClass("btn btn-warning btn-sm")->addButtonText("Warning")->button()->addButtonClass("btn btn-success btn-sm")->addButtonText("Success")->button();
        return $modal->getHTML();
    }
}

// verifica o status da transação...
// falta criar os métodos de atualização
// criar a classe Dao para generalizar acesso ao BD
// visualizar a lista de funcionários
