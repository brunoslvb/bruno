<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/component/ModalBox.php';

class ModalBoxModel extends CI_Model {

    public function modal(){
        $this->load->library('component/ModalBox', null, 'modal');

        $modal = new ModalBox();

        $modal->setHref('modal/modalDialog')->setBackground('0000FF')->addButtonText("Teste")->button();

        return $modal->getHTML();
    }
}

