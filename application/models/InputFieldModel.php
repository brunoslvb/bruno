<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/component/InputField.php';

class InputFieldModel extends CI_Model {

    public function inputField(){
        $this->load->library('component/InputField', null, 'input');

        $input = new InputField();

        $input->setForm()->inputLabel('Teste')->inputText('teste')->inputLabel('Bárbara')->inputText('LALA')->addButtonClass('btn btn-sm btn-warning mt-4')->addButtonText('Warning')->addButton();

        return $input->getHTML();
    }

    public function inputField2(){
        $this->load->library('component/InputField', null, 'input');

        $input = new InputField();

        $input->inputLabel('Teste')->inputText('teste')->inputLabel('LALA')->inputText('LALA');

        return $input->getHTML();
    }
}

