<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Componente extends MY_Controller{

	/**
	* Controller responsável por chamar as páginas dos
	* componentes com seu determinado conteúdo.
	*/


	function __construct() {
        parent::__construct();
        $this->load->model('ModalDialogModel', 'dialog');
        $this->load->model('ModalBoxModel', 'box');
        $this->load->library('ModalBox', 'modalbox');
        $this->load->model('InputFieldModel', 'model');
    }

    public function modalDialog(){

    	$data['modal'] = $this->dialog->modal();

        $html = $this->load->view('componente/modalDialog', $data, true);

        $this->show($html);
    }

    public function modalBox(){

    	$data['modal'] = $this->box->modal();

        $html = $this->load->view('componente/modalBox', $data, true);

        $this->show($html);
    }


    public function input(){

    	$data['input'] = $this->model->inputField();
    	$data['input2'] = $this->model->inputField2();

        $html = $this->load->view('componente/inputFields', $data, true);

        $this->show($html);
    }



}
