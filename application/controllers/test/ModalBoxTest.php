<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
require_once(APPPATH . '/libraries/component/ModalBox.php');

class ModalBoxTest extends MyToast{
	
	function __construct() {
		parent::__construct('ModalBox');
	}

	/** 
     * Função responsável por verificar se o link de 
     * redirecionamento do botão é inteiro.
     */ 
	function test_setHref(){
		$modal = new ModalBox();
		$modal->setHref(123);
		$href = $modal->getHref();
		$this->_assert_equals(is_int($href), false, "Url não é um valor inteiro: $href");
	}

	/** 
     * Função responsável por verificar se o background 
     * é um valor hexadecimal.
     */  
	function test_setBackground(){
		$modal = new ModalBox();
		$modal->setbackground('000000');
		$bg = $modal->getbackground();
		$this->_assert_equals(ctype_xdigit($bg), true, "Não é um número hexadecimal: ".$bg);
	}

	/** 
     * Função responsável por verificar se o título 
     * tem mais de 2 caracteres.
     */  
	function test_setTitle(){
		$modal = new ModalBox();
		$modal->setTitle('Fo');
		$title = $modal->getTitle();
		$this->_assert_equals(strlen($title) >= 3 ? true : false, true, "O texto título não pode ter menos de 3 caracteres: $title");
	}
}