<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
require_once(APPPATH . '/libraries/component/InputField.php');

class InputFieldTest extends MyToast{
	
	function __construct() {
		parent::__construct('InputField');
	}


	/**
	* Função responsável por verificar se o texto do botão 
	* tem no máximo 10 caracteres.
	*/
	function test_setButtonText(){
		$input = new InputField();
		$input->addButtonText('Button');
		$text =  $input->getButtonText();
		$this->_assert_true(strlen($text) <= 10, "Url não é um valor inteiro: $text");
	}


	/**
	* Função responsável por verificar se o texto do header
	* está em letras maiúsculas.
	*/
	function test_setTitle(){
		$input = new InputField();
		$input->labelHeader('TESTe');
		$title = $input->getLabelHeader();
		$this->_assert_true(ctype_upper($title), "O título deve ser em letras maiúsculas: ".$title);
	}


	/**
	* Função responsável por verificar se o form action 
	* está vazio.
	*/
	function test_setFormAction(){
		$input = new InputField();
		$input->formAction('');
		$action =  $input->getFormAction();
		$this->_assert_true($action != '', "O form action não pode ser vazio: $action");
	}
}