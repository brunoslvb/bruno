<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
require_once(APPPATH . '/libraries/component/ModalDialog.php');

class ModalDialogTest extends MyToast{
	
	function __construct() {
		parent::__construct('ModalDialog');
	}

	/** 
     * Função responsável por verificar se o tamando do 
     * modal foi setado como inteiro.
     */
	function test_setSize(){
		$modal = new ModalDialog();
		$modal->setSize('modal-sm');
		$size = $modal->getSize();
		$this->_assert_false(is_int($size), "Size não pode ser um valor inteiro: $size");
	}

	/** 
     * Função responsável por verificar se a posição do 
     * modal foi setada como inteiro.
     */
	function test_setPosition(){
		$modal = new ModalDialog();
		$modal->setPosition(2);
		$position = $modal->getPosition();
		$this->_assert_false(is_int($position), "Size não pode ser um valor inteiro: $position");
	}

	/** 
     * Função responsável por verificar se o título 
     * tem mais de 2 caracteres.
     */  
	function test_setTitle(){
		$modal = new ModalDialog();
		$modal->setTitle('Formulário');
		$title = $modal->getTitle();
		$this->_assert_true(strlen($title) >= 3, "O texto título não pode ter menos de 3 caracteres: $title");
	}


}