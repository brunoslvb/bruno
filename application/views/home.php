<!-- Main navigation -->
<header>
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top scrolling-navbar">
    <div class="container">
      <a class="navbar-brand" href="#"><strong>PHP - Bruno da Silva Barros</strong></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
        aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
        <ul class="navbar-nav mr-auto">

        </ul>

      </div>
    </div>
  </nav>
  <!-- Navbar -->
  <!-- Full Page Intro -->
  <div class="view  jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('https://wallpaperstream.com/wallpapers/full/gradient/Light-Gradient-Background.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    <!-- Mask & flexbox options-->
    <div class="mask rgba-purple-slight d-flex justify-content-center align-items-center">
      <!-- Content -->
      <div class="container">
        <!--Grid row-->
        <div class="row wow fadeIn">
          <!--Grid column-->
          <div class="col-md-12 text-center">
            <h1 class="display-4 font-weight-bold mb-0 pt-md-5 pt-5 wow fadeInUp">Projeto 2 - PHP</h1>
            <h5 class="pt-md-5 pt-sm-2 pt-5 pb-md-5 pb-sm-3 pb-5 wow fadeInUp white-text" data-wow-delay="0.2s">Componentes, testes unitários e Documentação com PHPDoc</h5>
            <div class="wow fadeInUp d-flex justify-content-center" data-wow-delay="0.4s">
				<div>
					<button class="btn btn-outline-black dropdown-toggle mr-4" id="componentes" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Componentes</button>

					<div class="dropdown-menu" id="componentes">
						<a class="dropdown-item" href="<?php echo base_url('componente/modalBox')?>">Modal Box</a>
						<a class="dropdown-item" href="<?php echo base_url('componente/modalDialog')?>">Modal Dialog</a>
						<a class="dropdown-item" href="<?php echo base_url('componente/input')?>">Input Fields</a>
					</div>
				</div>
				<div>
					<button class="btn btn-black dropdown-toggle mr-4" id="testes" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Testes</button>

					<div class="dropdown-menu" id="testes">
						<a class="dropdown-item" href="<?php echo base_url('test/modalBoxTest')?>">Modal Box Teste</a>
						<a class="dropdown-item" href="<?php echo base_url('test/modalDialogTest')?>">Modal Dialog Teste</a>
						<a class="dropdown-item" href="<?php echo base_url('test/inputFieldTest')?>">Input Fields Teste</a>
					</div>
				</div>
				<a class="" target="_blank" href="<?php echo base_url('docs/api')?>"><button class="btn btn-outline-black mr-4" type="button">PHPDOC</button>

            </div>
          </div>
          <!--Grid column-->
        </div>
        <!--Grid row-->
      </div>
      <!-- Content -->
    </div>
    <!-- Mask & flexbox options-->
  </div>
  <!-- Full Page Intro -->
</header>
<!-- Main navigation -->