<!--Navbar-->
<nav class="navbar navbar-expand-lg navbar-dark black">

  <!-- Navbar brand -->
  <a class="navbar-brand" href="#">PHP</a>

  <!-- Collapse button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="basicExampleNav">

    <!-- Links -->
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url('welcome/')?>">Home
        </a>
      </li>

      <!-- Dropdown -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="componentes" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">Componentes</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="componentes">
          <a class="dropdown-item" href="<?php echo base_url('componente/modalBox')?>">Modal Box</a>
            <a class="dropdown-item" href="<?php echo base_url('componente/modalDialog')?>">Modal Dialog</a>
            <a class="dropdown-item" href="<?php echo base_url('componente/input')?>">Input Fields</a>
        </div>
      </li>

      <!-- Dropdown -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="testes" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">Testes</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="testes">
          <a class="dropdown-item" href="<?php echo base_url('test/modalBoxTest')?>">Modal Box Teste</a>
            <a class="dropdown-item" href="<?php echo base_url('test/modalDialogTest')?>">Modal Dialog Teste</a>
            <a class="dropdown-item" href="<?php echo base_url('test/inputFieldTest')?>">Input Fields Teste</a>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" target="_" href="<?php echo base_url('docs/api') ?>">PHPDOC</a>
      </li>

    </ul>
    <!-- Links -->

  </div>
  <!-- Collapsible content -->

</nav>
<!--/.Navbar-->