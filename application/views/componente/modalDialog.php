
<div class="container col-md-8 mt-5">

	<span id="bebe"></span>

	<h2 class="">Modal Dialog</h2>
	<h3 class=""></h3>

	<p>Documentação do componente <i><b>Modal Dialog</b></i> utilizando o MDBootstrap.</p>

	<p>O Modal é uma caixa de diálogo/janela pop-up que pode ser utilizada para alertas, notificações do usuário, aprimoramentos da interface do usuário, formulários e muitos outros casos.</p>
	
	<p>O MDBootstrap nos disponibiliza algumas classes para manipularmos os componentes deste framework. Abaixo, estão alguns exemplos das classes. Com elas podemos posicionar, diminuir e aumentar o componente que estamos trabalhando.</p>

	<h5 class="mb-3"><b>Classes: </b></h5>

	<h6></h6>


	<div class="row">
		<div class="col-md-4">
			<ul>
				<li>Posição</li>
				<ul>
					<li>.modal-side .modal-top</li>
					<li>.modal-side .modal-bottom</li>
					<li>.modal-side .modal-left</li>
					<li>.modal-side .modal-right</li>
				</ul>
			</ul>
		</div>
		<div class="col-md-4">
			<ul>
				<li>Tamanho Modal</li>
				<ul>
					<li>.modal-sm</li>
					<li>.modal-lg</li>
					<li>.modal-xl</li>
					<li>.modal-full-height</li>
				</ul>
			</ul>
		</div>
		<div class="col-md-4">
			<ul>
				<li>Tamanho Botão</li>
				<ul>
					<li>.btn-sm</li>
					<li>.btn-lg</li>
				</ul>
			</ul>
		</div>
	</div>

	<div class="row mx-auto mt-4">
		<div class="col-md-5">
			<hr class="mt-4">
		</div>
		<div class="col-md-2">
			<h3>Exemplos</h3>
		</div>
		<div class="col-md-5">
			<hr class="mt-4">
		</div>
	</div>


	<div class="d-flex justify-content-around mt-4">
		<a class="btn btn-primary" data-toggle="modal" data-target="#modal">&lt;a&gt;</a>

		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal">&lt;button&gt;</button>
	</div>

	<div class="container border grey lighten-3 p-2 justify-content-start mb-5 mt-5">
		<div class="d-flex justify-content-end" style="margin-bottom: -15px">
			<h6><i><b>Utilizando a tag &lt;a&gt;</b></i></h6>
		</div>

		<hr> 

		<code><span class="black-text">1 | </span><span class="red-text">&lt;a</span>  <span class="green-text">class=</span><span class="orange-text">"btn btn-primary"</span> <span class="green-text">data-toggle=</span><span class="orange-text">"modal"</span> <span class="green-text">data-target=</span><span class="orange-text">"#modal"</span><span class="red-text">&gt;</span><span class="black-text">&lt;A&gt;</span><span class="red-text">&lt;/a&gt;</span></code><br>

	</div>

	
	<div class="container border grey lighten-3 p-2 justify-content-start mb-5 mt-5">

		<div class="d-flex justify-content-end" style="margin-bottom: -15px">
			<!-- <button type="button" class="btn btn-sm btn-black" onclick="copyToClipboard()">tete</button> -->
			<h6><i><b>Utilizando a tag &lt;button&gt;</b></i></h6>
		</div>

		<hr> 

		<code><span class="black-text">1 | </span><span class="red-text">&lt;button</span>  <span class="green-text">class=</span><span class="orange-text">"btn btn-success"</span> <span class="green-text">data-toggle=</span><span class="orange-text">"modal"</span> <span class="green-text">data-target=</span><span class="orange-text">"#modal"</span><span class="red-text">&gt;</span><span class="black-text">&lt;BUTTON&gt;</span><span class="red-text">&lt;/a&gt;</span></code><br>

	</div>


	<div class="container mx-auto mb-5 border hoverable p-3">
		<h6 class=""><i><b>Nota: </b></i>Observe que ambos os exemplos possuem os atributos: <code><span class="green-text">data-toggle=</span><span class="orange-text">"modal"</span> <span class="green-text">data-target=</span><span class="orange-text">"#modal"</span></code></h6>
		<h6 class="">Eles são obrigatórios para o funcionamento correto do <i><b>Modal Dialog</b></i>.</h6>
	</div>

	<hr>

	<h3 class="mt-4 mb-5 text-center"><b>Mais sobre Componentes</b></h3>

	<div class="d-flex justify-content-around mb-5">
		<a href="<?php echo base_url('componente/modalBox')?>"><button type="button" class="btn btn-sm btn-outline-black">Modal Box</button></a>
		<a href="<?php echo base_url('componente/input')?>"><button type="button" class="btn btn-sm btn-outline-black">Input Fields -> Forms</button></a>
	</div>


	<span class="mt-5"><?php echo $modal; ?></span>

</div>
