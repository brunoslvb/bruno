
<div class="container col-md-8 mt-5">

	<h2 class="">Modal Box</h2>
	<h3 class=""></h3>

	<p>Documentação do componente <i><b>Modal Box</b></i> utilizando o MDBootstrap.</p>

	<p>O Modal é uma caixa de diálogo/janela pop-up que pode ser utilizada para alertas, notificações do usuário, aprimoramentos da interface do usuário, formulários e muitos outros casos.</p>
	
	<p>O W3Schools não disponibiliza nenhuma classe para manipularmos o componente, porém abaixo, são listadas algumas funções criadas para fazer alterações simples como alterar o texto, mudar as cores do header e footer e adicionar links para os botões do Modal.</p>

	<h5 class="mb-3"><b>Funções: </b></h5>

	<h6></h6>


	<div class="row">
		<div class="col-md-6">
			<ul>
				<li><b>setTitle(string)</b></li>
				<ul>
					<li>Função recebendo uma string correspondente ao texto do Header do Modal.</li>
				</ul>
			</ul>

			<ul>
				<li><b>setContent(string)</b></li>
				<ul>
					<li>Função recebendo uma string correspondente ao conteúdo do Modal, podendo ser um outro componente.</li>
				</ul>
			</ul>
		</div>
		<div class="col-md-6">
			<ul>
				<li><b>setBackground(string)</b></li>
				<ul>
					<li>Função recebendo um valor hexadecimal correspondente ao header e footer do Modal.</li>
				</ul>
			</ul>

			<ul>
				<li><b>setHref(string, boolean)</b></li>
				<ul>
					<li>Função recebendo uma string e um valor boolean (fazendo referência ao link, se é local(true) ou externo(false)).</li>
				</ul>
			</ul>
		</div>
	</div>

	<div class="row mx-auto mt-4">
		<div class="col-md-5">
			<hr class="mt-4">
		</div>
		<div class="col-md-2">
			<h3>Exemplo</h3>
		</div>
		<div class="col-md-5">
			<hr class="mt-4">
		</div>
	</div>





	<div class="d-flex justify-content-around mt-4">
		<button id="myBtn">Abrir Modal</button>
	</div>

	<div class="container border grey lighten-3 p-2 justify-content-start mb-5 mt-5">
		<div class="d-flex justify-content-end" style="margin-bottom: -15px">
			<h6><i><b>Utilizando a tag &lt;button&gt;</b></i></h6>
		</div>

		<hr> 

		<code><span class="black-text">1 | </span><span class="red-text">&lt;button</span>  <span class="green-text">id=</span><span class="orange-text">"myBtn"</span><span class="red-text">&gt;</span><span class="black-text">Abrir Modal</span><span class="red-text">&lt;/button&gt;</span></code><br>

	</div>


	<div class="container mx-auto mb-5 border hoverable p-3">
		<h6 class=""><i><b>Nota: </b></i>Observe que no Modal Box é necessário o atributo "id" que faz referência ao Javascript que chama o Modal. Em nosso exemplo, o atributo é colocado na tag &lt;button&gt;, mas poderia ser inserido em qualquer outra tag.
		<h6 class="">Neste caso, o atributo é obrigatório para o funcionamento do <i><b>Modal Box</b></i>.</h6>
	</div>

	<hr>

	<h3 class="mt-4 mb-5 text-center"><b>Mais sobre Componentes</b></h3>

	<div class="d-flex justify-content-around mb-5">
		<a href="<?php echo base_url('componente/modalDialog')?>"><button type="button" class="btn btn-sm btn-outline-black">Modal Dialog</button></a>
		<a href="<?php echo base_url('componente/input')?>"><button type="button" class="btn btn-sm btn-outline-black">Input Fields -> Forms</button></a>
	</div>


	<span class="mt-5"><?php echo $modal; ?></span>

</div>
