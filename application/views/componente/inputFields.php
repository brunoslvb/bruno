
<div class="container col-md-8 mt-5">

	<h2 class="">Input Fields</h2>
	<h3 class=""></h3>

	<p>Documentação dos componentes de um formulário utilizando o MDBootstrap. Eles são chamados de <i><b>Input Fields</b></i>.</p>

	<p>Os Input Fields são campos de entrada de um formulário. Estes campos podem ser do tipo: text, number, date, file.</p>
	

	<h6 class="mb-3"><b>Para definir os campos de entrada com seus respectivos tipos, utilize as funções: </b></h6>
	
	<h5 class="mb-3">Funções: </h5>

	<div class="row">
		<div class="col-md-3">
			<ul>
				<li>Text</li>
				<ul>
					<li>inputText(string)</li>
				</ul>
			</ul>
		</div>
		<div class="col-md-3">
			<ul>
				<li>Number</li>
				<ul>
					<li>inputNumber(string)</li>
				</ul>
			</ul>
		</div>
		<div class="col-md-3">
			<ul>
				<li>Date</li>
				<ul>
					<li>inputDate(string)</li>
				</ul>
			</ul>
		</div>
		<div class="col-md-3">
			<ul>
				<li>File</li>
				<ul>
					<li>inputFile(string)</li>
				</ul>
			</ul>
		</div>
	</div>

	<div class="container mx-auto mb-5 mt-5 border hoverable p-3">
		<h6 class=""><i><b>Nota: </b></i>O parâmetro <i><b>string </b></i> é utilizado para identificar o campo de entrada. Esse paramêtro é colocado tanto no atributo <i><b>id </b></i> e <i><b>name </b></i>.</h6>
	</div>


	<!-- <p>O MDBootstrap nos disponibiliza algumas classes para manipularmos os componentes deste framework. Abaixo, estão alguns exemplos das classes. Com elas podemos posicionar, diminuir e aumentar o componente que estamos trabalhando.</p> -->

	<div class="row mx-auto mt-4">
		<div class="col-md-5">
			<hr class="mt-4">
		</div>
		<div class="col-md-2">
			<h3>Exemplos</h3>
		</div>
		<div class="col-md-5">
			<hr class="mt-4">
		</div>
	</div>

	<div class="col-md-6 mx-auto mt-5 mb-5 justify-content-around">
		<?php echo $input ?>
	</div>


	<div class="container border grey lighten-3 p-2 justify-content-start mb-5 mt-5">
		<div class="d-flex justify-content-end" style="margin-bottom: -15px">
			<h6><i><b>Com a função <i><b>setForm()</b></i></b></i></h6>
		</div>

		<hr> 

		<p>Para inserir a caixa do formulário, utilize a função <i><b>setForm()</b></i> na sua Model.</p>

	</div>

	

	<div class="col-md-6 mx-auto mt-5 mb-5 justify-content-around">
		<?php echo $input2 ?>
	</div>

	
	<div class="container border grey lighten-3 p-2 justify-content-start mb-5 mt-5">

		<div class="d-flex justify-content-end" style="margin-bottom: -15px">
			<!-- <button type="button" class="btn btn-sm btn-black" onclick="copyToClipboard()">tete</button> -->
			<h6><i><b>Sem a função <i><b>setForm()</b></i></b></i></h6>
		</div>

		<hr> 

		<p>Os campos de entrada serão incluídos sem nenhum problema mesmo sem a função <i><b>setForm()</b></i>.</p>

	</div>

	<hr>

	<h3 class="mt-4 mb-5 text-center"><b>Mais sobre Componentes</b></h3>

	<div class="d-flex justify-content-around mb-5">
		<a href="<?php echo base_url('componente/modalBox')?>"><button type="button" class="btn btn-sm btn-outline-black">Modal Box</button></a>
		<a href="<?php echo base_url('componente/modalDialog')?>"><button type="button" class="btn btn-sm btn-outline-black">Modal Dialog</button></a>
	</div>


</div>
